using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitList.Models
{
    public class FitUser
    {
        public int FitUserID { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public List<Excercise> FavoriteExcercises { get; set; }
    }

}
