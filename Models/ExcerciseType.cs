using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FitList.Models
{
    public class ExcerciseType
    {
        public int ExcerciseTypeID { get; set; }
        [StringLength(50, ErrorMessage = "Excercise type name cannot be longer than 50 characters.")]
        public string ExcerciseTypeName {get; set;}

        public string ExcerciseTypeDescription {get; set;}
    }
}
