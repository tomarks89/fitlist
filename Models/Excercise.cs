using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitList.Models
{
    public class Excercise
    {
        public int ExcerciseID { get; set; }
        public string ExcerciseName { get; set; }
        public string ExcerciseDescription { get; set; }
        public int DefaultReps { get; set; }
        public int DefaultSets { get; set; }

        public List<ExcerciseTask> ExcerciseTasks { get; set; }
    }
}
