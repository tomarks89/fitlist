using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitList.Models
{
    public class ExcerciseTask
    {
        public int ExcerciseTaskID { get; set; }
        public string ExcerciseTaskInstruction { get; set; }
        public int ExcerciseTaskReps { get; set; }

        public int ExcerciseID { get; set; }
        public Excercise Excercise { get; set; }
    }
}
