# Store

## Vuex

The Vuex constants in mutation-type.js, the Vuex modules in the subfolder modules (which are then loaded in the index.js).

### When Should I Use It

Although Vuex helps us deal with shared state management, it also comes with the cost of more concepts and boilerplate. It's a trade-off between short term and long term productivity.

![Vuex](https://vuex.vuejs.org/vuex.png)

If you've never built a large-scale SPA and jump right into Vuex, it may feel verbose and daunting. That's perfectly normal - if your app is simple, you will most likely be fine without Vuex. A simple store pattern may be all you need. But if you are building a medium-to-large-scale SPA, chances are you have run into situations that make you think about how to better handle state outside of your Vue components, and Vuex will be the natural next step for you.