﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FitList.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FitUsers",
                columns: table => new
                {
                    FitUserID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FitUsers", x => x.FitUserID);
                });

            migrationBuilder.CreateTable(
                name: "Excercises",
                columns: table => new
                {
                    ExcerciseID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ExcerciseName = table.Column<string>(nullable: true),
                    ExcerciseDescription = table.Column<string>(nullable: true),
                    DefaultReps = table.Column<int>(nullable: false),
                    DefaultSets = table.Column<int>(nullable: false),
                    FitUserID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Excercises", x => x.ExcerciseID);
                    table.ForeignKey(
                        name: "FK_Excercises_FitUsers_FitUserID",
                        column: x => x.FitUserID,
                        principalTable: "FitUsers",
                        principalColumn: "FitUserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExcerciseTasks",
                columns: table => new
                {
                    ExcerciseTaskID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ExcerciseTaskInstruction = table.Column<string>(nullable: true),
                    ExcerciseTaskReps = table.Column<int>(nullable: false),
                    ExcerciseID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExcerciseTasks", x => x.ExcerciseTaskID);
                    table.ForeignKey(
                        name: "FK_ExcerciseTasks_Excercises_ExcerciseID",
                        column: x => x.ExcerciseID,
                        principalTable: "Excercises",
                        principalColumn: "ExcerciseID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Excercises_FitUserID",
                table: "Excercises",
                column: "FitUserID");

            migrationBuilder.CreateIndex(
                name: "IX_ExcerciseTasks_ExcerciseID",
                table: "ExcerciseTasks",
                column: "ExcerciseID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExcerciseTasks");

            migrationBuilder.DropTable(
                name: "Excercises");

            migrationBuilder.DropTable(
                name: "FitUsers");
        }
    }
}
