using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FitList.Models;

namespace FitList.Helpers
{
    public class DataContext: DbContext
    {
        public DataContext (DbContextOptions<DataContext> options):base(options)
        {
            
        }

        public DbSet<FitUser> FitUsers { get; set; }

        public DbSet<Excercise> Excercises { get; set; }

        public DbSet<ExcerciseTask> ExcerciseTasks { get; set; }

    } 
}
