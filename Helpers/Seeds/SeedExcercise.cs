using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using FitList.Models;
using System.Linq;

namespace FitList.Helpers.Seeds
{
    public static class SeedExcercises
    {
        public static void Proseed(DataContext db)
        {
            if (db.Excercises.Any())
            {
                return;
            }

            db.Excercises.Add(new Excercise{
                ExcerciseName="Goblet Squat",
                ExcerciseDescription="Hold kettelbell and lock knees. Drop that ass.",
                DefaultReps=8,
                DefaultSets=3
                
                });

            db.SaveChanges();
        }
    }
}
