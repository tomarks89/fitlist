using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using FitList.Models;
using System.Linq;

namespace FitList.Helpers.Seeds
{
    public static class SeedFitUser
    {
        public static void Proseed(DataContext db)
        {
            if (db.FitUsers.Any())
            {
                return;
            }

            db.FitUsers.Add(new FitUser{
                FirstName = "Administrator",
                UserName = "admin",
                Password = "Admin123" });

            db.SaveChanges();
        }
    }
}
