using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace FitList.Helpers.Seeds
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var db = new DataContext(serviceProvider.GetRequiredService<DbContextOptions<DataContext>>()))
            {
                Seeds.SeedFitUser.Proseed(db);
                Seeds.SeedExcercises.Proseed(db);
            }
        }
    }
}
