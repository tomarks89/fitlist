﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FitList.Helpers;
using FitList.Models;

namespace FitList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FitUsersController : ControllerBase
    {
        private readonly DataContext _context;

        public FitUsersController(DataContext context)
        {
            _context = context;
        }

        // GET: api/FitUsers
        [HttpGet]
        public IEnumerable<FitUser> GetFitUsers()
        {
            return _context.FitUsers;
        }

        // GET: api/FitUsers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFitUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fitUser = await _context.FitUsers.FindAsync(id);

            if (fitUser == null)
            {
                return NotFound();
            }

            return Ok(fitUser);
        }

        // PUT: api/FitUsers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFitUser([FromRoute] int id, [FromBody] FitUser fitUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fitUser.FitUserID)
            {
                return BadRequest();
            }

            _context.Entry(fitUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FitUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FitUsers
        [HttpPost]
        public async Task<IActionResult> PostFitUser([FromBody] FitUser fitUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FitUsers.Add(fitUser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFitUser", new { id = fitUser.FitUserID }, fitUser);
        }

        // DELETE: api/FitUsers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFitUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fitUser = await _context.FitUsers.FindAsync(id);
            if (fitUser == null)
            {
                return NotFound();
            }

            _context.FitUsers.Remove(fitUser);
            await _context.SaveChangesAsync();

            return Ok(fitUser);
        }

        private bool FitUserExists(int id)
        {
            return _context.FitUsers.Any(e => e.FitUserID == id);
        }
    }
}